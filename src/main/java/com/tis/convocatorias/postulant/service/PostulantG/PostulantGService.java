package com.tis.convocatorias.postulant.service.PostulantG;


import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PostulantGService {


    private List<PostulantG> postulantg = new ArrayList<>(Arrays.asList(
            new PostulantG("1", "pepe", "Villegas", "Av. Oquendo 1234", 78945163, "pepe@gmail.com", "LCO-ADM", "Administrador de Laboratorio de Cómputo" ),
            new PostulantG("2", "mauricio", "Vargas", "Calle Antezana 1234", 71455163, "mauricio@gmail.com", "LCO-ADM", "Administrador de Laboratorio de Cómputo" ),
            new PostulantG("3", "rosa", "Villaroel", "Calle Lanza 1234", 71355163, "rosa@gmail.com", "LM-ADM=HW", "Administrador de Laboratorio de Mantenimiento de Hardware" ),
            new PostulantG("4", "felipe", "Tejerina", "Av. San Martin 1234", 75175163, "felipe@gmail.com", "LCO-ADM", "Administrador de Laboratorio de Cómputo" ),
            new PostulantG("5", "juana", "Huanca", "Av. Heroinas 1234", 79645163, "juana@gmail.com", "LM-ADM-SW", "Administrador de Laboratorio de Mantenimiento de Software" )
    ));

    public List<PostulantG> getAllPostulantg() {
        return postulantg;
    }

    public PostulantG getPostulantg(String id) {
        return postulantg.stream().filter(t -> t.getId_postulations().equals(id)).findFirst().get();

    }

    public void addPostulantg(PostulantG postulantG) {
        postulantg.add(postulantG);
    }

    public void updatePostulantg(String id, PostulantG postulantG) {
        for (int i = 0; i < postulantg.size(); i++) {
            PostulantG p = postulantg.get(i);
            if (p.getId_postulations().equals(id)) {
                postulantg.set(i, postulantG);
                return;
            }

        }

    }

    public void deletePostulantg(String id) {
        postulantg.removeIf(p -> p.getId_postulations().equals(id));
    }
}
