package com.tis.convocatorias.postulant.service.Postulantes;

import com.tis.convocatorias.postulant.service.Announcement.Announcement;
import com.tis.convocatorias.postulant.service.Auxiliature.Auxiliary;
import com.tis.convocatorias.postulant.service.Person.Person;

public class Postulantes {

    private String id_postulantes;
    private Announcement announcement;
    private Auxiliary auxiliature;
    private Person person;
    private boolean status;
    private int ci;
    private int nroDocumentos;
    private String carrera;


    public Postulantes() {
    }

    public Postulantes(String id_postulantes, Announcement announcement, Auxiliary auxiliature, Person person, boolean status, int ci, int nroDocumentos, String carrera) {
        this.id_postulantes = id_postulantes;
        this.announcement = announcement;
        this.auxiliature = auxiliature;
        this.person = person;
        this.status = status;
        this.ci = ci;
        this.nroDocumentos = nroDocumentos;
        this.carrera = carrera;
    }

    public String getId_postulantes() {
        return id_postulantes;
    }

    public void setId_postulantes(String id_postulantes) {
        this.id_postulantes = id_postulantes;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public Auxiliary getAuxiliature() {
        return auxiliature;
    }

    public void setAuxiliature(Auxiliary auxiliature) {
        this.auxiliature = auxiliature;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public int getNroDocumentos() {
        return nroDocumentos;
    }

    public void setNroDocumentos(int nroDocumentos) {
        this.nroDocumentos = nroDocumentos;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
}
