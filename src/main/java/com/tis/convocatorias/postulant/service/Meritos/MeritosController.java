package com.tis.convocatorias.postulant.service.Meritos;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class MeritosController {

    @Autowired
    private MeritosService meritosService;

    @RequestMapping("/meritos")
    public List<Meritos> getAllMeritos() {
        return meritosService.getAllMeritos();
    }

    @RequestMapping("/meritos/{id}")
    public Meritos getMeritos(@PathVariable String id) {
        return meritosService.getMeritos(id);
    }

    @PostMapping("/meritos")
    public void addMeritos(@RequestBody Meritos meritos) {
        meritosService.addMeritos(meritos);
    }

    @PutMapping("/meritos/{id}")
    public void updateMeritos(@RequestBody Meritos meritos, @PathVariable String id) {
        meritosService.updateMeritos(id, meritos);
    }

    @DeleteMapping("/meritos/{id}")
    public void deleteMeritos(@PathVariable String id) {
        meritosService.deleteMeritos(id);
    }
}
