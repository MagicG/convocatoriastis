package com.tis.convocatorias.postulant.service.PostulantG;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class PostulantGController {

    @Autowired
    private PostulantGService postulantGService;

    @RequestMapping("/postulations")
    public List<PostulantG> getAllPostulantg() {
        return postulantGService.getAllPostulantg();
    }

    @RequestMapping("/postulations/{id}")
    public PostulantG getPostulantg(@PathVariable String id) {
        return postulantGService.getPostulantg(id);
    }

    @PostMapping("/postulations")
    public void addPostulantg(@RequestBody PostulantG postulantG) {
        postulantGService.addPostulantg(postulantG);
    }

    @PutMapping("/postulations/{id}")
    public void updatePostulantg(@RequestBody PostulantG postulantg, @PathVariable String id) {
        postulantGService.updatePostulantg(id, postulantg);
    }

    @DeleteMapping("/postulations/{id}")
    public void deletePostulantg(@PathVariable String id) {
        postulantGService.deletePostulantg(id);
    }
}
