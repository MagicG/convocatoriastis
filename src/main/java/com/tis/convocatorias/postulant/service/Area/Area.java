package com.tis.convocatorias.postulant.service.Area;

public class Area {

    private String id_area;
    private String name;

    public Area() {
    }

    public Area(String id_area, String name) {
        this.id_area = id_area;
        this.name = name;
    }

    public String getId_area() {
        return id_area;
    }

    public void setId_area(String id_area) {
        this.id_area = id_area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
