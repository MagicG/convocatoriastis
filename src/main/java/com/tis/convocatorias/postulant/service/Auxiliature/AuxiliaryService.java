package com.tis.convocatorias.postulant.service.Auxiliature;


import com.tis.convocatorias.postulant.service.Academic_unit.Academic_unit;
import com.tis.convocatorias.postulant.service.Area.Area;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AuxiliaryService {


    private Academic_unit aunit1 = new Academic_unit("1", "Departamento de Informatica Sistemas");
    private Area a1 = new Area("1", "Laboratorio");

    private List<Auxiliary> auxiliary = new ArrayList<>(Arrays.asList(
            new Auxiliary("1","Administrador de Laboratorio de Cómputo","LCO-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("2","Administrador de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("3","Auxiliar de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("4","Administrador de Laboratorio de Mantenimiento de Software","LM-ADM-SW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("5","Auxiliar de Laboratorio de Mantenimiento de Software","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("6","Administrador de Laboratorio de Desarrollo","LDS-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("7","Auxiliar de Terminal de Laboratorio de Cómputo","LDS-ATL", "80 Hrs/mes", aunit1, a1)
    ));

    private List<Auxiliary> auxiliary1 = new ArrayList<>(Arrays.asList(
            new Auxiliary("6","Administrador de Laboratorio de Cómputo","LCO-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("7","Administrador de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("8","Auxiliar de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("9","Administrador de Laboratorio de Mantenimiento de Software","LM-ADM-SW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("10","Auxiliar de Laboratorio de Mantenimiento de Software","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("11","Administrador de Laboratorio de Desarrollo","LDS-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("12","Auxiliar de Terminal de Laboratorio de Cómputo","LDS-ATL", "80 Hrs/mes", aunit1, a1)
    ));

    private List<Auxiliary> auxiliary2 = new ArrayList<>(Arrays.asList(
            new Auxiliary("13","Administrador de Laboratorio de Cómputo","LCO-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("14","Administrador de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("15","Auxiliar de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("16","Administrador de Laboratorio de Mantenimiento de Software","LM-ADM-SW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("17","Auxiliar de Laboratorio de Mantenimiento de Software","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("18","Administrador de Laboratorio de Desarrollo","LDS-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("19","Auxiliar de Terminal de Laboratorio de Cómputo","LDS-ATL", "80 Hrs/mes", aunit1, a1)
    ));

    private List<Auxiliary> auxiliary3 = new ArrayList<>(Arrays.asList(
            new Auxiliary("20","Administrador de Laboratorio de Cómputo","LCO-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("21","Administrador de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("22","Auxiliar de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("23","Administrador de Laboratorio de Mantenimiento de Software","LM-ADM-SW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("24","Auxiliar de Laboratorio de Mantenimiento de Hardware","LM-ADM-HW", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("25","Administrador de Laboratorio de Desarrollo","LDS-ADM", "80 Hrs/mes", aunit1, a1),
            new Auxiliary("26","Auxiliar de Terminal de Laboratorio de Cómputo","LDS-ATL", "80 Hrs/mes", aunit1, a1)
    ));

    public List<Auxiliary> getAllAuxiliary() {
        return auxiliary;
    }

    public List<Auxiliary> getAuxiliary(String id) {
        List<Auxiliary> aux = new List<Auxiliary>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Auxiliary> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Auxiliary auxiliature) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Auxiliary> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Auxiliary> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Auxiliary get(int index) {
                return null;
            }

            @Override
            public Auxiliary set(int index, Auxiliary element) {
                return null;
            }

            @Override
            public void add(int index, Auxiliary element) {

            }

            @Override
            public Auxiliary remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Auxiliary> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Auxiliary> listIterator(int index) {
                return null;
            }

            @Override
            public List<Auxiliary> subList(int fromIndex, int toIndex) {
                return null;
            }


        };
        //return auxiliatures.stream().filter(t -> t.getId_auxiliary().equals(id)).findFirst().get();
        if (id.equals("1")){
            aux = auxiliary1;
        }else if (id.equals("2")){
            aux = auxiliary2;
        }else if (id.equals("3")){
            aux = auxiliary3;
        }
        return aux;
    }

    public void addAuxiliary(Auxiliary auxiliary) {
        this.auxiliary.add(auxiliary);
    }

    public void updateAuxiliary(String id, Auxiliary auxiliature) {
        for (int i = 0; i < auxiliary.size(); i++) {
            Auxiliary p = auxiliary.get(i);
            if (p.getId_auxiliary().equals(id)) {
                auxiliary.set(i, auxiliature);
                return;
            }

        }

    }

    public void deleteAuxiliary(String id) {
        auxiliary.removeIf(p -> p.getId_auxiliary().equals(id));
    }
}
