package com.tis.convocatorias.postulant.service.postulant;

import com.tis.convocatorias.postulant.service.Announcement.Announcement;
import com.tis.convocatorias.postulant.service.Auxiliature.Auxiliary;
import com.tis.convocatorias.postulant.service.Person.Person;

public class Postulant {

    private String id_postulant;
    private Announcement announcement;
    private Auxiliary auxiliature;
    private Person person;
    private boolean status;


    public Postulant() {
    }

    public Postulant(String id_postulant, Announcement announcement, Auxiliary auxiliature, Person person, boolean status) {
        this.id_postulant = id_postulant;
        this.announcement = announcement;
        this.auxiliature = auxiliature;
        this.person = person;
        this.status = status;
    }

    public String getId_postulant() {
        return id_postulant;
    }

    public void setId_postulant(String id_postulant) {
        this.id_postulant = id_postulant;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public Auxiliary getAuxiliature() {
        return auxiliature;
    }

    public void setAuxiliature(Auxiliary auxiliature) {
        this.auxiliature = auxiliature;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
