package com.tis.convocatorias.postulant.service.postulant;


import com.tis.convocatorias.postulant.service.Academic_unit.Academic_unit;
import com.tis.convocatorias.postulant.service.Announcement.Announcement;
import com.tis.convocatorias.postulant.service.Area.Area;
import com.tis.convocatorias.postulant.service.Auxiliature.Auxiliary;
import com.tis.convocatorias.postulant.service.Management.Management;
import com.tis.convocatorias.postulant.service.Person.Person;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PostulantService {


    private Management man1 = new Management("1","2020");
    private Academic_unit aunit1 = new Academic_unit("1", "Departamento de Informatica Sistemas");
    private Area a1 = new Area("1", "Laboratorio");
    private Announcement ann1 = new Announcement("1", "CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTAR A AUXILIATURAS EN LABORATORIO DE COMPUTACIÓN, DE MANTENIMIENTO Y DESARROLLO",
                                                "El Departamento de Informática y Sistemas junto a las Carreras de Ing. Informática e Ing. de Sistemas, de la Facultad de Ciencias y Tecnología, convoca al concurso de méritos y examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los requerimientos que se tienen programados para la gestión 2020.",
                                                "INFSIS-LAB-2020", "07/02/2020 11:00", "Los Honorables Consejos de Carrera de Informática y Sistemas designarán respectivamente; para la calificación de méritos 1 docente y 1 delegado estudiante, de la misma manera para la comisión de conocimientos cada consejo designará 1 docente y un estudiante veedor por cada temática.",
                                                "Una vez concluido el proceso, la jefatura  decidirá qué auxiliares serán seleccionados para cada ítem, considerando los resultados finales y  las necesidades propias de cada laboratorio. Los nombramientos de auxiliar universitario titular recaerán sobre aquellos postulantes que hubieran aprobado y obtenido mayor calificación. Caso contrario se procederá con el nombramiento de aquel que tenga la calificación mayor como auxiliar invitado. Cabe resaltar que un auxiliar invitado solo tendrá nombramiento por los periodos académicos del semestre I y II de 2020.",
                                                aunit1, man1, a1);
    private Auxiliary aux1 = new Auxiliary("5", "Administrador de Laboratorio de Cómputo", "LCO-ADM", "80 Hrs/mes", aunit1, a1);
    private Person per1 = new Person("3", "Rosa", "Martinez", "Av. Oquendo 3514", 73249814, "rosita@gmail.com");

    private List<Postulant> postulants = new ArrayList<>(Arrays.asList(
            new Postulant( "1", ann1, aux1, per1, true ),
            new Postulant("2", ann1, aux1, per1, false ),
            new Postulant("3", ann1, aux1, per1, false ),
            new Postulant("4", ann1, aux1, per1, true ),
            new Postulant("5", ann1, aux1, per1, true )
    ));

    private List<Postulant> postulants1 = new ArrayList<>(Arrays.asList(
            new Postulant( "6", ann1, aux1, per1, true ),
            new Postulant("7", ann1, aux1, per1, false ),
            new Postulant("8", ann1, aux1, per1, false ),
            new Postulant("9", ann1, aux1, per1, true ),
            new Postulant("10", ann1, aux1, per1, true )
    ));

    private List<Postulant> postulants2 = new ArrayList<>(Arrays.asList(
            new Postulant( "11", ann1, aux1, per1, true ),
            new Postulant("12", ann1, aux1, per1, false ),
            new Postulant("13", ann1, aux1, per1, false ),
            new Postulant("14", ann1, aux1, per1, true ),
            new Postulant("15", ann1, aux1, per1, true )
    ));
    private List<Postulant> postulants3 = new ArrayList<>(Arrays.asList(
            new Postulant( "16", ann1, aux1, per1, true ),
            new Postulant("17", ann1, aux1, per1, false ),
            new Postulant("18", ann1, aux1, per1, false ),
            new Postulant("19", ann1, aux1, per1, true ),
            new Postulant("20", ann1, aux1, per1, true )
    ));

    public List<Postulant> getAllPostulants() {
        return postulants;
    }

    public List<Postulant> getPostulant(String id) {
         List<Postulant> aux = new List<Postulant>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Postulant> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Postulant postulant) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Postulant> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Postulant> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Postulant get(int index) {
                return null;
            }

            @Override
            public Postulant set(int index, Postulant element) {
                return null;
            }

            @Override
            public void add(int index, Postulant element) {

            }

            @Override
            public Postulant remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Postulant> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Postulant> listIterator(int index) {
                return null;
            }

            @Override
            public List<Postulant> subList(int fromIndex, int toIndex) {
                return null;
            }
        };
        //return postulants.stream().filter(t -> t.getId_postulant().equals(id)).findFirst().get();
        if(id.equals("1")) {
            aux = postulants1;
        }else if (id.equals("2")){
            aux = postulants2;
        }else if(id.equals("3")){
            aux = postulants3;
        }
        return aux;
    }

    public void addPostulant(Postulant postulant) {
        postulants.add(postulant);
    }

    public void updatePostulant(String id, Postulant postulant) {
        for (int i = 0; i < postulants.size(); i++) {
            Postulant p = postulants.get(i);
            if (p.getId_postulant().equals(id)) {
                postulants.set(i, postulant);
                return;
            }

        }

    }

    public void deletePostulant(String id) {
        postulants.removeIf(p -> p.getId_postulant().equals(id));
    }
}
