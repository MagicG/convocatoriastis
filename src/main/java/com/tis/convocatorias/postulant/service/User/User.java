package com.tis.convocatorias.postulant.service.User;

import com.tis.convocatorias.postulant.service.Announcement.Announcement;

public class User {

    private String id_user;
    private String role;
    private String identity_card;
    private String password;
    private Announcement announcement;

    public User() {
    }

    public User(String id_user, String role, String identity_card, String password, Announcement announcement) {
        this.id_user = id_user;
        this.role = role;
        this.identity_card = identity_card;
        this.password = password;
        this.announcement = announcement;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getIdentity_card() {
        return identity_card;
    }

    public void setIdentity_card(String identity_card) {
        this.identity_card = identity_card;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }
}
