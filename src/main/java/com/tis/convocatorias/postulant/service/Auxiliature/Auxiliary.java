package com.tis.convocatorias.postulant.service.Auxiliature;

import com.tis.convocatorias.postulant.service.Academic_unit.Academic_unit;
import com.tis.convocatorias.postulant.service.Area.Area;
import com.tis.convocatorias.postulant.service.postulant.Postulant;

public class Auxiliary {

    private String id_auxiliary;
    private String name;
    private String code;
    private String academic_hours;
    private Academic_unit academic_unit;
    private Area area;

    public Auxiliary() {
    }

    public Auxiliary(String id_auxiliary, String name, String code, String academic_hours, Academic_unit academic_unit, Area area) {
        this.id_auxiliary = id_auxiliary;
        this.name = name;
        this.code = code;
        this.academic_hours = academic_hours;
        this.academic_unit = academic_unit;
        this.area = area;
    }

    public String getId_auxiliary() {
        return id_auxiliary;
    }

    public void setId_auxiliary(String id_auxiliary) {
        this.id_auxiliary = id_auxiliary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAcademic_hours() {
        return academic_hours;
    }

    public void setAcademic_hours(String academic_hours) {
        this.academic_hours = academic_hours;
    }

    public Academic_unit getAcademic_unit() {
        return academic_unit;
    }

    public void setAcademic_unit(Academic_unit academic_unit) {
        this.academic_unit = academic_unit;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }
}
