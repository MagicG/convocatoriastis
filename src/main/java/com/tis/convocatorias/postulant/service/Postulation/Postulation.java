package com.tis.convocatorias.postulant.service.Postulation;

public class Postulation {

    private String id_postulation;
    private String announcement;
    private String auxiliature_name;
    private String aux_codItem;



    public Postulation() {
    }

    public Postulation(String id_postulation, String announcement, String auxiliature_name, String aux_codItem) {
        this.id_postulation = id_postulation;
        this.announcement = announcement;
        this.auxiliature_name = auxiliature_name;
        this.aux_codItem = aux_codItem;
    }

    public String getId_postulation() {
        return id_postulation;
    }

    public void setId_postulation(String id_postulation) {
        this.id_postulation = id_postulation;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String getAuxiliature_name() {
        return auxiliature_name;
    }

    public void setAuxiliature_name(String auxiliature_name) {
        this.auxiliature_name = auxiliature_name;
    }

    public String getAux_codItem() {
        return aux_codItem;
    }

    public void setAux_codItem(String aux_codItem) {
        this.aux_codItem = aux_codItem;
    }
}
