package com.tis.convocatorias.postulant.service.Meritos;


import com.tis.convocatorias.postulant.service.Person.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class MeritosService {


//    private Management man1 = new Management("1","2020");
//    private Academic_unit aunit1 = new Academic_unit("1", "Departamento de Informatica Sistemas");
//    private Area a1 = new Area("1", "Laboratorio");
//    private Announcement ann1 = new Announcement("1", "CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTAR A AUXILIATURAS EN LABORATORIO DE COMPUTACIÓN, DE MANTENIMIENTO Y DESARROLLO",
//                                                "El Departamento de Informática y Sistemas junto a las Carreras de Ing. Informática e Ing. de Sistemas, de la Facultad de Ciencias y Tecnología, convoca al concurso de méritos y examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los requerimientos que se tienen programados para la gestión 2020.",
//                                                "INFSIS-LAB-2020", "07/02/2020 11:00", "Los Honorables Consejos de Carrera de Informática y Sistemas designarán respectivamente; para la calificación de méritos 1 docente y 1 delegado estudiante, de la misma manera para la comisión de conocimientos cada consejo designará 1 docente y un estudiante veedor por cada temática.",
//                                                "Una vez concluido el proceso, la jefatura  decidirá qué auxiliares serán seleccionados para cada ítem, considerando los resultados finales y  las necesidades propias de cada laboratorio. Los nombramientos de auxiliar universitario titular recaerán sobre aquellos postulantes que hubieran aprobado y obtenido mayor calificación. Caso contrario se procederá con el nombramiento de aquel que tenga la calificación mayor como auxiliar invitado. Cabe resaltar que un auxiliar invitado solo tendrá nombramiento por los periodos académicos del semestre I y II de 2020.",
//                                                aunit1, man1, a1);
//    private Auxiliature aux1 = new Auxiliature("5", "Administrador de Laboratorio de Cómputo", "LCO-ADM", "80 Hrs/mes", aunit1, a1);
    private Person per1 = new Person("3", "Rosa", "Martinez", "Av. Oquendo 3514", 73249814, "rosita@gmail.com");

    private List<Meritos> meritos = new ArrayList<>(Arrays.asList(
            new Meritos( "1","La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final.",
                    100,80, 50, "1",per1 ),
            new Meritos("1","La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final.",
                    100,80, 57, "1",per1),
            new Meritos("1","La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final.",
                    100,80, 70, "2",per1),
            new Meritos("1","La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final.",
                    100,80, 65, "2",per1),
            new Meritos("1","La calificación de méritos se basará en los documentos presentados por el postulante y se realizará sobre la base de 100 puntos que representa el 20% del puntaje final.",
                    100,80, 78, "1",per1)
    ));


    public List<Meritos> getAllMeritos() {
        return meritos;
    }

    public Meritos getMeritos(String id) {
        return meritos.stream().filter(t -> t.getId_merit().equals(id)).findFirst().get();

    }

    public void addMeritos(Meritos meritos) {
        this.meritos.add(meritos);
    }

    public void updateMeritos(String id, Meritos meritos) {
        for (int i = 0; i < this.meritos.size(); i++) {
            Meritos p = this.meritos.get(i);
            if (p.getId_merit().equals(id)) {
                this.meritos.set(i, meritos);
                return;
            }

        }

    }

    public void deleteMeritos(String id) { meritos.removeIf(p -> p.getId_merit().equals(id));
    }
}
