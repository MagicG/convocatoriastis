package com.tis.convocatorias.postulant.service.Conocimientos;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class ConocimientosController {

    @Autowired
    private ConocimientosService conocimientosService;

    @RequestMapping("/conocimientos")
    public List<Conocimientos> getAllConocimientos() {
        return conocimientosService.getAllConocimientos();
    }

    @RequestMapping("/conocimientos/{id}")
    public Conocimientos getConocimientos(@PathVariable String id) {
        return conocimientosService.getConocimientos(id);
    }

    @PostMapping("/conocimientos")
    public void addConocimientos(@RequestBody Conocimientos conocimientos) {
        conocimientosService.addConocimientos(conocimientos);
    }

    @PutMapping("/conocimientos/{id}")
    public void updateConocimientos(@RequestBody Conocimientos conocimientos, @PathVariable String id) {
        conocimientosService.updateConocimientos(id, conocimientos);
    }

    @DeleteMapping("/conocimientos/{id}")
    public void deleteConocimientos(@PathVariable String id) {
        conocimientosService.deleteConocimientos(id);
    }
}
