package com.tis.convocatorias.postulant.service.Postulantes;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class PostulantesController {

    @Autowired
    private PostulantesService postulantesService;

    @RequestMapping("/postulantes")
    public List<Postulantes> getAllPostulantes() {
        return postulantesService.getAllPostulantes();
    }

    @RequestMapping("/postulantes/{id}")
    public Postulantes getPostulantes(@PathVariable String id) {
        return postulantesService.getPostulantes(id);
    }

    @PostMapping("/postulantes")
    public void addPostulantes(@RequestBody Postulantes postulantes) {
        postulantesService.addPostulantes(postulantes);
    }

    @PutMapping("/postulantes/{id}")
    public void updatePostulantes(@RequestBody Postulantes postulantes, @PathVariable String id) {
        postulantesService.updatePostulantes(id, postulantes);
    }

    @DeleteMapping("/postulantes/{id}")
    public void deletePostulantes(@PathVariable String id) {
        postulantesService.deletePostulantes(id);
    }
}
