package com.tis.convocatorias.postulant.service.Meritos;

import com.tis.convocatorias.postulant.service.Person.Person;

public class Meritos {

    private String id_merit;
    private String description;
    private int base_score;
    private int percentage;
    private int final_score;
    private String id_announ;
    private Person person;


    public Meritos() {
    }

    public Meritos(String id_merit, String description, int base_score, int percentage, int final_score, String id_announ, Person person) {
        this.id_merit = id_merit;
        this.description = description;
        this.base_score = base_score;
        this.percentage = percentage;
        this.final_score = final_score;
        this.id_announ = id_announ;
        this.person = person;
    }

    public String getId_merit() {
        return id_merit;
    }

    public void setId_merit(String id_merit) {
        this.id_merit = id_merit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBase_score() {
        return base_score;
    }

    public void setBase_score(int base_score) {
        this.base_score = base_score;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public int getFinal_score() {
        return final_score;
    }

    public void setFinal_score(int final_score) {
        this.final_score = final_score;
    }

    public String getId_announ() {
        return id_announ;
    }

    public void setId_announ(String id_announ) {
        this.id_announ = id_announ;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
