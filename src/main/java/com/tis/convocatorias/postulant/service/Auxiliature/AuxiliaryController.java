package com.tis.convocatorias.postulant.service.Auxiliature;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class AuxiliaryController {

    @Autowired
    private AuxiliaryService auxiliaryService;

    @RequestMapping("/auxiliary")
    public List<Auxiliary> getAllAuxiliary() {
        return auxiliaryService.getAllAuxiliary();
    }

    @RequestMapping("/auxiliary/{id}")
    public List<Auxiliary> getAuxiliary(@PathVariable String id) {
        return auxiliaryService.getAuxiliary(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/auxiliary")
    public void addAuxiliary(@RequestBody Auxiliary auxiliary) {
        auxiliaryService.addAuxiliary(auxiliary);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/auxiliary/{id}")
    public void updateAuxiliary(@RequestBody Auxiliary auxiliary, @PathVariable String id) {
        auxiliaryService.updateAuxiliary(id, auxiliary);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/auxiliary/{id}")
    public void deleteAuxiliary(@PathVariable String id) {
        auxiliaryService.deleteAuxiliary(id);
    }
}
