package com.tis.convocatorias.postulant.service.Conocimientos;


import com.tis.convocatorias.postulant.service.Person.Person;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ConocimientosService {


//    private Management man1 = new Management("1","2020");
//    private Academic_unit aunit1 = new Academic_unit("1", "Departamento de Informatica Sistemas");
//    private Area a1 = new Area("1", "Laboratorio");
//    private Announcement ann1 = new Announcement("1", "CONVOCATORIA A CONCURSO DE MÉRITOS Y PRUEBAS DE CONOCIMIENTOS PARA OPTAR A AUXILIATURAS EN LABORATORIO DE COMPUTACIÓN, DE MANTENIMIENTO Y DESARROLLO",
//                                                "El Departamento de Informática y Sistemas junto a las Carreras de Ing. Informática e Ing. de Sistemas, de la Facultad de Ciencias y Tecnología, convoca al concurso de méritos y examen de competencia para la provisión de Auxiliares del Departamento, tomando como base los requerimientos que se tienen programados para la gestión 2020.",
//                                                "INFSIS-LAB-2020", "07/02/2020 11:00", "Los Honorables Consejos de Carrera de Informática y Sistemas designarán respectivamente; para la calificación de méritos 1 docente y 1 delegado estudiante, de la misma manera para la comisión de conocimientos cada consejo designará 1 docente y un estudiante veedor por cada temática.",
//                                                "Una vez concluido el proceso, la jefatura  decidirá qué auxiliares serán seleccionados para cada ítem, considerando los resultados finales y  las necesidades propias de cada laboratorio. Los nombramientos de auxiliar universitario titular recaerán sobre aquellos postulantes que hubieran aprobado y obtenido mayor calificación. Caso contrario se procederá con el nombramiento de aquel que tenga la calificación mayor como auxiliar invitado. Cabe resaltar que un auxiliar invitado solo tendrá nombramiento por los periodos académicos del semestre I y II de 2020.",
//                                                aunit1, man1, a1);
//    private Auxiliature aux1 = new Auxiliature("5", "Administrador de Laboratorio de Cómputo", "LCO-ADM", "80 Hrs/mes", aunit1, a1);
    private Person per1 = new Person("3", "Rosa", "Martinez", "Av. Oquendo 3514", 73249814, "rosita@gmail.com");

    private List<Conocimientos> conocimientos = new ArrayList<>(Arrays.asList(
            new Conocimientos( "1","La calificación de conocimientos se realiza sobre la base de 100 puntos, equivalentes al 80% de la calificación final.",
                    100,80, 50, "1",per1 ),
            new Conocimientos("1","La calificación de conocimientos se realiza sobre la base de 100 puntos, equivalentes al 80% de la calificación final.",
                    100,80, 60, "1",per1),
            new Conocimientos("1","La calificación de conocimientos se realiza sobre la base de 100 puntos, equivalentes al 80% de la calificación final.",
                    100,80, 57, "2",per1),
            new Conocimientos("1","La calificación de conocimientos se realiza sobre la base de 100 puntos, equivalentes al 80% de la calificación final.",
                    100,80, 65, "2",per1),
            new Conocimientos("1","La calificación de conocimientos se realiza sobre la base de 100 puntos, equivalentes al 80% de la calificación final.",
                    100,80, 78, "1",per1)
    ));

    public List<Conocimientos> getAllConocimientos() {
        return conocimientos;
    }

    public Conocimientos getConocimientos(String id) {
        return conocimientos.stream().filter(t -> t.getId_knowledge().equals(id)).findFirst().get();

    }

    public void addConocimientos(Conocimientos conocimientos) {
        this.conocimientos.add(conocimientos);
    }

    public void updateConocimientos(String id, Conocimientos conocimientos) {
        for (int i = 0; i < this.conocimientos.size(); i++) {
            Conocimientos p = this.conocimientos.get(i);
            if (p.getId_knowledge().equals(id)) {
                this.conocimientos.set(i, conocimientos);
                return;
            }

        }

    }

    public void deleteConocimientos(String id) {
        conocimientos.removeIf(p -> p.getId_knowledge().equals(id));
    }
}
