package com.tis.convocatorias.postulant.service.Management;

public class Management {

    private String id_management;
    private String period;

    public Management() {
    }

    public Management(String id_management, String period) {
        this.id_management = id_management;
        this.period = period;
    }

    public String getId_management() {
        return id_management;
    }

    public void setId_management(String id_management) {
        this.id_management = id_management;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
