package com.tis.convocatorias.postulant.service.Postulation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class PostulationController {

    @Autowired
    private PostulationService postulationService;

    @RequestMapping("/requirements")
    public List<Postulation> getAllPostulations() {
        return postulationService.getAllPostulations();
    }

    @RequestMapping("/requirements/{id}")
    public Postulation getPostulation(@PathVariable String id) {
        return postulationService.getPostulation(id);
    }

    @PostMapping("/requirements")
    public void addPostulation(@RequestBody Postulation postulation) {
        postulationService.addPostulation(postulation);
    }

    @PutMapping("/requirements/{id}")
    public void updatePostulation(@RequestBody Postulation postulation, @PathVariable String id) {
        postulationService.updatePostulation(id, postulation);
    }

    @DeleteMapping("/requirements/{id}")
    public void deletePostulation(@PathVariable String id) {
        postulationService.deletePostulation(id);
    }
}
