package com.tis.convocatorias.postulant.service.Person;

public class Person {

    private String id_person;
    private String name;
    private String surname;
    private String address;
    private int phone;
    private String email;

    public Person() {
    }

    public Person(String id_person, String name, String surname, String address,
                  int phone, String email) {
        super();
        this.id_person = id_person;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.phone = phone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getId_person() {
        return id_person;
    }

    public void setId_person(String id_person) {
        this.id_person = id_person;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surname;
    }

    public void setSurName(String lastName) {
        this.surname = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
