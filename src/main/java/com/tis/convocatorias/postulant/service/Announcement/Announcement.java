package com.tis.convocatorias.postulant.service.Announcement;

import com.tis.convocatorias.postulant.service.Academic_unit.Academic_unit;
import com.tis.convocatorias.postulant.service.Area.Area;
import com.tis.convocatorias.postulant.service.Management.Management;

public class Announcement {

    private String id_announcement;
    private String title;
    private String description;
    private String code;
    private String deadline;
    private String courts_description;
    private String appointment;
    private Academic_unit academic_unit;
    private Management management;
    private Area area;

    public Announcement() {
    }

    public Announcement(String id_announcement, String title, String description, String code, String deadline, String courts_description, String appointment, Academic_unit academic_unit, Management management, Area area) {
        this.id_announcement = id_announcement;
        this.title = title;
        this.description = description;
        this.code = code;
        this.deadline = deadline;
        this.courts_description = courts_description;
        this.appointment = appointment;
        this.academic_unit = academic_unit;
        this.management = management;
        this.area = area;
    }

    public String getId_announcement() {
        return id_announcement;
    }

    public void setId_announcement(String id_announcement) {
        this.id_announcement = id_announcement;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getCourts_description() {
        return courts_description;
    }

    public void setCourts_description(String courts_description) {
        this.courts_description = courts_description;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public Academic_unit getAcademic_unit() {
        return academic_unit;
    }

    public void setAcademic_unit(Academic_unit academic_unit) {
        this.academic_unit = academic_unit;
    }

    public Management getManagement() {
        return management;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }
}
