package com.tis.convocatorias.postulant.service.postulant;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class PostulantController {

    @Autowired
    private PostulantService postulantService;

    @RequestMapping("/postulants")
    public List<Postulant> getAllPostulants() {
        return postulantService.getAllPostulants();
    }

    @RequestMapping("/postulants/{id}")
    public List<Postulant> getPostulant(@PathVariable String id) {
        return postulantService.getPostulant(id);
    }

    @PostMapping("/postulants")
    public void addPostulant(@RequestBody Postulant postulant) {
        postulantService.addPostulant(postulant);
    }

    @PutMapping("/postulants/{id}")
    public void updatePostulant(@RequestBody Postulant postulant, @PathVariable String id) {
        postulantService.updatePostulant(id, postulant);
    }

    @DeleteMapping("/postulants/{id}")
    public void deletePostulant(@PathVariable String id) {
        postulantService.deletePostulant(id);
    }
}
