package com.tis.convocatorias.postulant.service.Academic_unit;

public class Academic_unit {

    private String id_academic_unit;
    private String name;

    public Academic_unit() {
    }

    public Academic_unit(String id_academic_unit, String name) {
        this.id_academic_unit = id_academic_unit;
        this.name = name;
    }

    public String getId_academic_unit() {
        return id_academic_unit;
    }

    public void setId_academic_unit(String id_academic_unit) {
        this.id_academic_unit = id_academic_unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
