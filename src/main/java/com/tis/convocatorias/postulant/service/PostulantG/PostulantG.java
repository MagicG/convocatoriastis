package com.tis.convocatorias.postulant.service.PostulantG;

public class PostulantG {

    private String id_postulations;
    private String name;
    private String surname;
    private String address;
    private int phone;
    private String email;
    private String codItem;
    private String auxiliary;

    public PostulantG() {
    }

    public PostulantG(String id_person, String name, String surname, String address, int phone, String email, String codItem, String auxiliary) {
        this.id_postulations = id_person;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.codItem = codItem;
        this.auxiliary = auxiliary;
    }

    public String getId_postulations() {
        return id_postulations;
    }

    public void setId_postulations(String id_person) {
        this.id_postulations = id_person;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCodItem() {
        return codItem;
    }

    public void setCodItem(String codItem) {
        this.codItem = codItem;
    }

    public String getAuxiliary() {
        return auxiliary;
    }

    public void setAuxiliary(String auxiliary) {
        this.auxiliary = auxiliary;
    }
}
